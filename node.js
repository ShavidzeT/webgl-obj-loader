var fs = require('fs');
var OBJ = require('webgl-obj-loader');

var meshPath = './development/models/Banana.obj';
var opt = { encoding: 'utf8' };

fs.readFile(meshPath, opt, function (err, data){
  if (err) return console.error(err);
  var mesh = new OBJ.Mesh(data);
  console.log(mesh)
});